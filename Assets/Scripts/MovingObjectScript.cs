﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObjectScript : MonoBehaviour {
   
        public static int zAxisPos = 0; //Static because other scripts may need to get this.
        public float xAxisBoundry = 7.5f;
        public float speed = 10f;
        public bool isClicked;

    private void Start()
    {
        isClicked = false;
    }
    void Update()
        {
        if (this.isClicked) { 
            Vector3 mouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, zAxisPos - Camera.main.transform.position.z);
            mouse = Camera.main.ScreenToWorldPoint(mouse);

            this.transform.position = new Vector3(mouse.x, mouse.y, zAxisPos);
        }
    }

    public void OnMouseDown()
    {
        if (this.isClicked == true)
        {
            this.isClicked = false;
        } else
        {
            this.isClicked = true;
        }
    }
}
